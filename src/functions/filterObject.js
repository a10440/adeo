/**
 *
 * @param name
 * @param people
 * @param value
 * @param predicate
 * @returns {*[]|{name, people: *[]}|null}
 */
const filterObject = ({ name, people = [] }, value) => {
  if (!value) {
    throw new Error('A value must be provided to filter data');
  }
  let i = -1;
  let res = Array();
  if (Array.isArray(people) && people.length > 0) {
    while (++i < people.length) {
      if (people[i]) {
        const { name, animals } = people[i];
        ((animals) => {
          animals.length > 0 ? res.push({ name, animals }) : null;
        })(animals.filter(({ name }) => name.match(value)));
      }
    }
    return res.length > 0 ? { name, people: res } : null;
  }
  return [];
};

module.exports = filterObject;
