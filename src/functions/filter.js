const filterObject = require('./filterObject');
/**
 *
 * @param array
 * @param value
 * @returns {(*|{name, people: *[]})[]}
 */
const filter = (array, value) => {
  if (Array.isArray(array) && array.length > 0) {
    return array
      .map((elt) => (elt ? filterObject(elt, value) : elt))
      .filter((elt) => elt);
  }
  return [];
};
module.exports = filter;
