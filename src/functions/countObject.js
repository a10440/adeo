/**
 *
 * @param name
 * @param people
 * @returns {{}}
 */
const countObject = ({ name, people = [] }) => {
  if (Array.isArray(people) && people.length > 0) {
    const peoples = people
      .map((value) => {
        if (value) {
          const { name, animals } = value;
          return ((animals) => {
            return animals && animals.length >= 0
              ? { name: `${name} [${animals.length}]`, animals }
              : null;
          })(animals);
        }
      })
      .filter((elt) => elt);
    return {
      name: `${name} [${peoples.length}]`,
      people: [...peoples],
    };
  }
};

module.exports = countObject;
