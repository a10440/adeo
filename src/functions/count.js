const countObject = require('./countObject');
/**
 *
 * @param array
 * @returns {*[]|*}
 */
const count = (array) => {
  if (Array.isArray(array) && array.length > 0) {
    return array
      .map((elt) => (elt ? countObject(elt) : elt))
      .filter((elt) => elt);
  }
  return [];
};
module.exports = count;
