const CommandEmitter = require('./event.handler');

/**
 *
 * @param cmd
 * @returns {*}
 */
const extractCMDArg = (cmd) => (cmd && cmd.match('=') ? cmd.split('=')[1] : cmd);

/**
 *
 * @param cmd
 * @returns {*}
 */
const formatCommand = (cmd) =>
  !!cmd && cmd.match('^--')
    ? (() => {
        const cmdPlusArg = cmd.slice(2, cmd.length);
        return cmdPlusArg.match('=') ? cmdPlusArg.split('=')[0] : cmdPlusArg;
      })()
    : cmd;

module.exports = {
  sendCommand: (cmd) => {
    CommandEmitter.emit(formatCommand(cmd), extractCMDArg(cmd));
  },
};
