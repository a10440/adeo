let depth = 0;
const evalObject = (data) => {
  let defMap = {};
  let i = -1;
  if (data.length > 0 && Array.isArray(data)) {
    const first = data[0];
    const props = Object.keys(first);
    while (++i < props.length) {
      const nodeValue = first[props[i]];
      if (Array.isArray(nodeValue)) {
        depth++;
        return {
          ...defMap,
          [props[i]]: { type: 'array', children: evalObject(nodeValue), depth },
        };
      } else {
        defMap[props[i]] = { type: typeof nodeValue };
      }
    }
    return defMap;
  }
  return {};
};

module.exports = evalObject;
