const assert = require('assert/strict');
const {
  inputWithAllProperties,
  inputWithNullsInside,
  inputWithOneAnimalsChild,
  empty,
} = require('../data/inputs');
const count = require('../../src/functions/count');

assert.deepEqual(count(empty), []);
assert.deepEqual(count(inputWithNullsInside), [
  {
    name: 'Stash [1]',
    people: [
      {
        name: 'Benoît [2]',
        animals: [{ name: 'Chameleons' }, { name: 'Bee-eater' }],
      },
    ],
  },
]);
assert.deepEqual(count(inputWithAllProperties), [
  {
    name: 'Tohal [2]',
    people: [
      {
        name: 'Effie Houghton [7]',
        animals: [
          {
            name: 'Zebra',
          },
          {
            name: 'Ring-tailed Lemur',
          },
          {
            name: 'Fly',
          },
          {
            name: 'Blue Iguana',
          },
          {
            name: 'Emu',
          },
          {
            name: 'African Wild Ass',
          },
          {
            name: 'Numbat',
          },
        ],
      },
      {
        name: 'Essie Bennett [7]',
        animals: [
          {
            name: 'Aldabra Tortoise',
          },
          {
            name: 'Patagonian Toothfish',
          },
          {
            name: 'Giant Panda',
          },
          {
            name: 'Goat',
          },
          {
            name: 'Quahog',
          },
          {
            name: 'Collared Lemur',
          },
          {
            name: 'Aldabra Tortoise',
          },
        ],
      },
    ],
  },
  {
    name: 'Uoe [2]',
    people: [
      {
        name: 'Harold Patton [8]',
        animals: [
          {
            name: 'Bearded Dragon',
          },
          {
            name: 'Chicken',
          },
          {
            name: 'Sand Cat',
          },
          {
            name: 'Hedgehog',
          },
          {
            name: 'Collared Lemur',
          },
          {
            name: 'Frogmouth',
          },
          {
            name: 'Raccoon dog',
          },
          {
            name: 'Shortfin Mako Shark',
          },
        ],
      },
      {
        name: 'Millie Lapini [8]',
        animals: [
          {
            name: 'Bearded Dragon',
          },
          {
            name: 'Peafowl',
          },
          {
            name: 'Aardvark',
          },
          {
            name: 'Cows',
          },
          {
            name: 'Crane Fly',
          },
          {
            name: 'Rock Hyrax',
          },
          {
            name: 'Gerbils',
          },
          {
            name: 'Brown Bear',
          },
        ],
      },
    ],
  },
]);
assert.deepEqual(count(inputWithOneAnimalsChild), [
  {
    name: 'Stash [2]',
    people: [
      {
        name: 'Houghton [0]',
        animals: [],
      },
      {
        name: 'Benoît [2]',
        animals: [
          {
            name: 'Chameleons',
          },
          {
            name: 'Bee-eater',
          },
        ],
      },
    ],
  },
]);
