const assert = require('assert/strict');
const {
  inputWithAllProperties,
  inputWithNullsInside,
  inputWithOneAnimalsChild,
  empty,
} = require('../data/inputs');
const filter = require('../../src/functions/filter');

assert.deepEqual(filter(empty, ''), []);
assert.deepEqual(filter(inputWithNullsInside, 'on'), [
  {
    name: 'Stash',
    people: [
      {
        name: 'Benoît',
        animals: [
          {
            name: 'Chameleons',
          },
        ],
      },
    ],
  },
]);
assert.deepEqual(filter(inputWithAllProperties, 'y'), [
  {
    name: 'Tohal',
    people: [
      {
        animals: [
          {
            name: 'Fly',
          },
        ],
        name: 'Effie Houghton',
      },
    ],
  },
  {
    name: 'Uoe',
    people: [
      {
        animals: [
          {
            name: 'Crane Fly',
          },
          {
            name: 'Rock Hyrax',
          },
        ],
        name: 'Millie Lapini',
      },
    ],
  },
]);
assert.deepEqual(filter(inputWithOneAnimalsChild, 'on'), [
  {
    name: 'Stash',
    people: [
      {
        name: 'Benoît',
        animals: [
          {
            name: 'Chameleons',
          },
        ],
      },
    ],
  },
]);
