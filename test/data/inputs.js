const inputWithAllProperties = [
  {
    name: 'Tohal',
    people: [
      {
        name: 'Effie Houghton',
        animals: [
          { name: 'Zebra' },
          { name: 'Ring-tailed Lemur' },
          { name: 'Fly' },
          { name: 'Blue Iguana' },
          { name: 'Emu' },
          { name: 'African Wild Ass' },
          { name: 'Numbat' },
        ],
      },
      {
        name: 'Essie Bennett',
        animals: [
          { name: 'Aldabra Tortoise' },
          { name: 'Patagonian Toothfish' },
          { name: 'Giant Panda' },
          { name: 'Goat' },
          { name: 'Quahog' },
          { name: 'Collared Lemur' },
          { name: 'Aldabra Tortoise' },
        ],
      },
    ],
  },
  {
    name: 'Uoe',
    people: [
      {
        name: 'Harold Patton',
        animals: [
          { name: 'Bearded Dragon' },
          { name: 'Chicken' },
          { name: 'Sand Cat' },
          { name: 'Hedgehog' },
          { name: 'Collared Lemur' },
          { name: 'Frogmouth' },
          { name: 'Raccoon dog' },
          { name: 'Shortfin Mako Shark' },
        ],
      },
      {
        name: 'Millie Lapini',
        animals: [
          { name: 'Bearded Dragon' },
          { name: 'Peafowl' },
          { name: 'Aardvark' },
          { name: 'Cows' },
          { name: 'Crane Fly' },
          { name: 'Rock Hyrax' },
          { name: 'Gerbils' },
          { name: 'Brown Bear' },
        ],
      },
    ],
  },
];
const inputWithOneAnimalsChild = [
  {
    name: 'Stash',
    people: [
      {
        name: 'Houghton',
        animals: [],
      },
      {
        name: 'Benoît',
        animals: [{ name: 'Chameleons' }, { name: 'Bee-eater' }],
      },
    ],
  },
];

const inputWithNullsInside = [
  {
    name: 'Stash',
    people: [
      null,
      null,
      {
        name: 'Benoît',
        animals: [{ name: 'Chameleons' }, { name: 'Bee-eater' }],
      },
    ],
  },
];
const empty = [];

module.exports = {
  inputWithAllProperties,
  inputWithOneAnimalsChild,
  inputWithNullsInside,
  empty
};
