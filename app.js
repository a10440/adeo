const commandHandler = require('./src/lib/event.handler');
const { sendCommand } = require('./src/lib/commands');
const { data } = require('./src/data/input');
const filter = require('./src/functions/filter');
const count = require('./src/functions/count');

const [, , ...args] = process.argv;
const commands = [
  { cmd: 'filter', handler: filter },
  { cmd: 'count', handler: count },
];

commands.forEach(({ cmd, handler }) =>
  commandHandler.on(cmd, (arg) =>
    console.log(
      JSON.stringify(arg ? handler(data, arg) : handler(data))
    )
  )
);
args.forEach((arg) => sendCommand(arg));
